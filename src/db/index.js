const mysql = require("mysql");

let pool;

const init = dbConfig => {
  pool = mysql.createPool(dbConfig);
};

const query = (text, params) => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        return reject(err);
      }
      connection.query(text, params, (error, result) => {
        connection.release();
        if (error) return reject(error);
        resolve(result);
      });
    });
  });
};

const commit = conn =>
  new Promise((resolve, reject) => {
    conn.commit(err => {
      if (err) {
        return reject(err);
      }
      resolve(true);
    });
  });

const rollback = (conn, err) =>
  new Promise(resolve => {
    conn.rollback(() => {
      resolve(err);
    });
  });

const beginTransaction = conn =>
  new Promise((resolve, reject) => {
    conn.beginTransaction(err => {
      if (err) {
        conn.release();
        return reject(err);
      }
      resolve(conn);
    });
  });

const ping = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) return reject(err);
      connection.ping(err => {
        connection.release();
        if (err) return reject(err);
        console.log("Db instance connected.");
        resolve();
      });
    });
  });
};

const cleanup = () => {
  return new Promise((resolve, reject) => {
    pool.end(err => {
      if (err) reject(err);
      resolve();
    });
  });
};

const getConnection = () => {
  return new Promise((resolve, reject) => {
    pool.getConnection((err, connection) => {
      if (err) {
        return reject(err);
      }
      return resolve(connection);
    });
  });
};

const queryWithConnection = (conn, text, params) => {
  return new Promise((resolve, reject) => {
    conn.query(text, params, (error, result) => {
      if (error) return reject(error);
      resolve(result);
    });
  });
};

module.exports = {
  query,
  ping,
  rollback,
  beginTransaction,
  commit,
  init,
  pool,
  cleanup,
  getConnection,
  queryWithConnection
};
