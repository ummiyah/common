const setResponse = async (ctx, next) => {
  const response = await next();
  if (ctx.status === "401" || ctx.status === 401) {
    return;
  }
  ctx.status = 200;
  ctx.body = response;
};

module.exports = setResponse;
