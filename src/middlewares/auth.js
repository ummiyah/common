const jsonwebtoken = require("jsonwebtoken");
const Boom = require("boom");

const verifyTokenAndGetUser = async (authorizationHeader, JWT_SECRET) =>
  new Promise((resolve, reject) => {
    if (!authorizationHeader || authorizationHeader.length < 2)
      reject(Boom.unauthorized("authorization header missing"));
    const token = authorizationHeader.split(" ")[1];
    jsonwebtoken.verify(token, JWT_SECRET, (err, decoded) => {
      if (err) reject(Boom.unauthorized("token verification failed"));
      return resolve(decoded);
    });
  });

const verifyAuth = async (ctx, next) => {
  try {
    const authHeader = ctx.request.header["authorization"];
    const token = authHeader.split(" ");
    if (token.length < 2) throw Boom.unauthorized("token missing in header");
    const decoded = jsonwebtoken.verify(token[1], process.env.JWT_SECRET);
    // TODO: Implement checks from https://firebase.google.com/docs/auth/admin/verify-id-tokens
    ctx.user = decoded;
    return await next();
  } catch (e) {
    console.log(e);
    if (!Boom.isBoom(e)) throw Boom.unauthorized("error decoding token");
    else throw e;
  }
};

const verifyServiceAuth = async (ctx, next) => {
  try {
    const authHeader = ctx.request.header["authorization"];
    const token = authHeader.split(" ");
    if (token.length < 2) throw Boom.unauthorized("token missing in header");
    const decoded = jsonwebtoken.verify(token[1], process.env.JWT_SECRET);
    if (!decoded.iss === "api.trell.co" || !decoded.role === "service") throw Boom.unauthorized("not service");
    return await next();
  } catch (e) {
    if (!Boom.isBoom(e)) throw Boom.unauthorized("not service");
    throw e;
  }
};

module.exports = {
  verifyTokenAndGetUser,
  verifyAuth,
  verifyServiceAuth
};
