module.exports = {
  auth: require("./auth"),
  errorHandler: require("./errorHandler"),
  router: require("./router"),
  setResponse: require("./setResponse")
};
