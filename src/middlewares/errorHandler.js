const Boom = require("boom");

const errorHandler = async (ctx, next) => {
  try {
    await next();
  } catch (err) {
    console.log(err);
    if (Boom.isBoom(err)) {
      ctx.status = err.output.payload.statusCode;
      ctx.body = err.output.payload.message;
    } else {
      const error = Boom.badImplementation("Internal Server Error");
      ctx.status = error.output.payload.statusCode;
      ctx.body = error.output.payload.message;
    }
    ctx.app.emit("error", err, ctx);
  }
};

module.exports = errorHandler;
