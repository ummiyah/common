const compose = require("koa-compose");
const Boom = require("boom");

const getApiRouter = (apiRouter, routers) => {
  Object.keys(routers).forEach(key => {
    const router = routers[key];
    apiRouter.use(
      router.routes(),
      router.allowedMethods({
        throw: true,
        notImplemented: () => Boom.notImplemented(),
        methodNotAllowed: () => Boom.methodNotAllowed()
      })
    );
  });
  return apiRouter;
};

const combineRouter = (apiRouter, routers) => {
  const apiRouterInner = getApiRouter(apiRouter, routers);
  const routerToReturn = [];
  routerToReturn.push(
    apiRouterInner.routes(),
    apiRouterInner.allowedMethods({
      throw: true,
      notImplemented: () => Boom.notImplemented(),
      methodNotAllowed: () => Boom.methodNotAllowed()
    })
  );
  return routerToReturn;
};

const composedRouter = (apiRouter, routers) => compose(combineRouter(apiRouter, routers));

module.exports = composedRouter;
