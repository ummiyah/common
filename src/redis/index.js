const Redis = require("ioredis");
const Boom = require("boom");

const redis = {};

const init = async redisConf => {
  redis.client = new Redis(redisConf);
};

const rget = key =>
  new Promise((resolve, reject) => {
    redis.client.get(key, (err, response) => {
      if (err) return reject(err);
      return resolve(response);
    });
  });

const rset = (key, data, expiry) =>
  new Promise((resolve, reject) => {
    redis.client.set(key, data, "EX", expiry, (err, response) => {
      if (err) return reject(err);
      return resolve(response);
    });
  });

const ping = async () => {
  await redis.client.ping();
  console.log("Redis connected");
};

/**
 * Utility to check if a key is in redis cache. If it is not, database is called with queryString
 * and queryParams. If firstResult is set, the first row of db call result is used.
 * @param {string} redisKey key redis will use to set a value on
 * @param {string} queryString database query string
 * @param {Any} queryParams see node-mysql escaped query params
 * @param {Boolean} firstResult whether or not you want to return result[0](first row) after db call
 * @param {Number} redis expiry in seconds, defaults to 1 day.
 */
const checkCacheElseQueryAndSet = async (
  db,
  redisKey,
  queryString,
  queryParams,
  firstResult,
  expiry = 60 * 5
) => {
  const rResult = await rget(redisKey);
  if (rResult != null) return JSON.parse(rResult);

  const result = await db.query(queryString, queryParams);

  if (result.length) {
    if (firstResult) rset(redisKey, JSON.stringify(result[0]), expiry);
    // No need to wait, can happen async.
    else rset(redisKey, JSON.stringify(result), expiry);
  } else {
    throw Boom.notFound();
  }

  // Conditionally send the first row or whole result
  return firstResult ? result[0] : result;
};

redis.rget = rget;
redis.rset = rset;
redis.ping = ping;
redis.checkCacheElseQueryAndSet = checkCacheElseQueryAndSet;
redis.init = init;
redis.client = null;

module.exports = redis;
