module.exports = {
  db: require("./src/db"),
  middlewares: require("./src/middlewares"),
  redis: require("./src/redis")
};
